import Phaser from "phaser"
import Media from "../atoms/Media"
import GameSound from "../atoms/Sound"
import SceneKeys from "../constants/SceneKeys"
import TextureKeys from "../constants/TextureKeys"

export default class GameOver extends Phaser.Scene {
  private background!: Phaser.GameObjects.Image
  private logoEncanto!: Phaser.GameObjects.Image
  private logoPg!: Phaser.GameObjects.Image
  private videoContainer!: Media
  private txtPrelaunch!: Phaser.GameObjects.Image
  private btnReplay!: Phaser.GameObjects.Image
  private btnLearnMore!: Phaser.GameObjects.Image
  public soundContainer!: GameSound
  private countMatch: number = 0
  constructor() {
    super(SceneKeys.GameOver)
  }

  init (data: {countMatch: number}) {
    this.countMatch = data.countMatch
  }
  preload() {
    if (this.countMatch === 9) {
      this.add.image(330, 380, TextureKeys.TxtWin)
      .setOrigin(0.5, 0.5).setDepth(1)
    } else { 
      this.add.image(330, 380, TextureKeys.TxtLose)
      .setOrigin(0.5, 0.5).setDepth(1)
      this.add.text(330, 445, `${this.countMatch} / 9`, {
        fontSize: '32px',
        color: '#00ffff',
        shadow: {fill: true, blur: 0, offsetY: 0},
      }).setOrigin(0.5).setDepth(1)
    }
  }

  create() {

    let width = this.cameras.main.width;
    let height = this.cameras.main.height;
    this.background = this.add.image(0, 0, TextureKeys.OverBackground)
      .setOrigin(0, 0)
    this.background.displayWidth = width
    this.background.displayHeight = height 
    const xRatio = width / this.background.width
    const yRatio = height / this.background.height
    this.logoEncanto = this.add.image(330, 100, TextureKeys.LogoEncanto)
      .setOrigin(0.5, 0.5)
    this.logoEncanto.displayWidth *= 0.7
    this.logoEncanto.displayHeight *= 0.7
    this.add.image(width - 12, height - 8, TextureKeys.Legal)
      .setOrigin(1,1)
    this.logoPg = this.add.image(12, height - 8, TextureKeys.LogoPg)
      .setOrigin(0,1)
    this.logoPg.displayWidth = this.logoPg.width * xRatio
    this.logoPg.displayHeight = this.logoPg.height * yRatio 

    this.videoContainer = new Media({
      scene: this,
      x: width / 2,
      y: 594,
      frameKey: TextureKeys.VideoFrame,
      mediaKey: TextureKeys.VideoPostLaunch
    })
    this.videoContainer.displayWidth *= 0.85
    this.videoContainer.displayHeight *= 0.85

    this.soundContainer = new GameSound({
      scene: this as Phaser.Scene,
      x: 616,
      y: 69,
      onKey: TextureKeys.SoundOn,
      offKey: TextureKeys.SoundOff,
      audioKey: 'soundtrack'
    })
    // this.soundContainer.soundtrack.stop()

    //
    this.btnReplay = this.add.image(
      width / 2, 750, TextureKeys.TxtPrelaunch
    ).setOrigin(0.5, 0.5).setInteractive({cursor: "pointer"})
    this.btnReplay.displayWidth *= 0.9
    this.btnReplay.displayHeight *= 0.9

    this.btnReplay = this.add.image(
      width / 3 - 30, 800, TextureKeys.BtnReplay
    ).setOrigin(0.5, 0.5).setInteractive({cursor: "pointer"})
    this.btnReplay.displayWidth *= 0.8
    this.btnReplay.displayHeight *= 0.8
    this.btnReplay.on("pointerdown", (pointer: any) => {
      this.scene.start(SceneKeys.Game)
    })

    this.btnLearnMore = this.add.image(
      width * 2 / 3 + 30, 800, TextureKeys.BtnLearnMore
    ).setOrigin(0.5, 0.5).setInteractive({cursor: "pointer"})
    this.btnLearnMore.displayWidth *= 0.8
    this.btnLearnMore.displayHeight *= 0.8
    this.btnLearnMore.on("pointerdown", (pointer: any) => {
      // let gameScene = this.scene.get(SceneKeys.Game)
      // gameScene.scene.restart()
      this.scene.start()
    })
  }

  update() {
    const v = this.videoContainer
    const s = this.soundContainer
    if (v.media.video.ended ) {
      s.soundtrack.play()
    } else {
      s.soundtrack.stop()
    }
  }

}

