import Phaser from "phaser"
import SceneKeys from "../constants/SceneKeys"
import TextureKeys from "../constants/TextureKeys"

export default class Preloader extends Phaser.Scene {
  private background!: Phaser.GameObjects.Image
  constructor() {
    super(SceneKeys.Preloader)
  }

  preload() {
    this.load.image(TextureKeys.PreloadBackground, 'assets/bg_1.jpg')
    this.load.image(TextureKeys.LogoEncanto, 'assets/logo_encanto.png')
    this.load.image(TextureKeys.LogoPg, 'assets/logo_pg.png')
    this.load.image(TextureKeys.Legal, 'assets/legal.png')
    this.load.audio('soundtrack', 'assets/soundtrack.mp3')
  }


  create() {
    this.scene.start(SceneKeys.Loading)
  }

}

