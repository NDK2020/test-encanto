import Phaser from 'phaser';
import Character from '../atoms/Character';
import Mask from '../atoms/Mask';
import GameSound from '../atoms/Sound';
import Timer from '../atoms/Time';
import SceneKeys from '../constants/SceneKeys';
import TextureKeys from '../constants/TextureKeys';

export default class DemoScene extends Phaser.Scene {
  private footer!: Phaser.GameObjects.Image
  private gameBackground!: Phaser.GameObjects.Image
  private logoEncanto!: Phaser.GameObjects.Image
  private logoPg!: Phaser.GameObjects.Image
  private closeSign!: Phaser.GameObjects.Image
  private cards: Phaser.GameObjects.Image[] = []
  private curMatchId!: number
  public countMatch: number = 0
  private dragWs: number[] = []
  private dragHs: number[] = []
  private timerContainer!: Phaser.GameObjects.Container
  private timerState: "pause" | "running" | "end" = "running"
  public soundContainer!: GameSound
  public initTime: number = 30
  public timeUp!: Mask
  constructor() {
    super(SceneKeys.Game);
  }

  preload() {
    this.cards = []
    this.dragHs = []
    this.dragHs = []
    this.initTime = 30
    this.countMatch = 0
  }

  create() {
    const width = this.scale.width
    const height = this.scale.height
    this.gameBackground = this.add.image(0, 0, TextureKeys.GameBackground)
      .setOrigin(0,0)
    const xRatio = width / this.gameBackground.width
    const yRatio = height / this.gameBackground.height
    this.gameBackground.displayWidth = width
    this.gameBackground.displayHeight = height 

    this.footer = this.add.image(0, height - 175, TextureKeys.GameFooter)
      .setOrigin(0,0)
    this.footer.displayWidth = width

    // this.add.image(40, 20, TextureKeys.LogoEncanto)
    //   .setOrigin(0,0)

    this.add.image(width / 2, height - 170, TextureKeys.TxtTutorial)
      .setOrigin(0.5, 0.5)
    this.logoPg = this.add.image(12, height - 8, TextureKeys.LogoPg)
      .setOrigin(0,1)
    this.logoPg.displayWidth = this.logoPg.width * xRatio
    this.logoPg.displayHeight = this.logoPg.height * yRatio 

    this.add.image(width - 12, height - 8, TextureKeys.Legal)
      .setOrigin(1,1)

    //timer
    this.timerContainer = new Timer({
      scene: this,
      x: 470,
      y: 75,
      key: TextureKeys.Time,
      state: "running",
      initTime: 60
    })

    this.soundContainer = new GameSound({
      scene: this as Phaser.Scene,
      x: 616,
      y: 69,
      onKey: TextureKeys.SoundOn,
      offKey: TextureKeys.SoundOff,
      audioKey: 'soundtrack'
    })

    for (let i = 1; i <= 9; i++) {
      let x = 336; let y = 563; let addX = 9; let addY = 20 
      if (i == 2) {
        x = 372; y = 135; addX = 5; addY = 5
      } else if (i == 3) {
        x = 513; y = 518; addX = 5; addY = 5
      } else if (i == 4) {
        x = 509; y = 308; addX = 5; addY = 5
      } else if (i == 5) {
        x = 124; y = 179; addX = 10; addY = 16 
      } else if (i == 6) {
        x = 258; y = 126; addX = 14; addY =13 
      } else if (i == 7) {
        x = 253; y = 250; addX = 5; addY = 5
      } else if (i == 8) {
        x = 391; y = 337; addX = 5; addY = 16 
      } else if (i == 9) {
        x = 160; y = 440; addX = 5; addY = 5
      }
      const tmp = this.add.image(
        x,
        y,
        TextureKeys.Characters.onGame[`${i}`],
      ).setOrigin(0.5, 0).setInteractive()

      // console.log(`${i}: ${tmp.width}`)
      // console.log(`${i}: ${tmp.height}`)
      tmp.displayWidth -=  addX
      tmp.displayHeight -=  addY
      this.dragWs.push(tmp.displayWidth)
      this.dragHs.push(tmp.displayHeight)
      tmp.setTint(0x000000)
      tmp.name = `${i}`
      tmp.input.dropZone = true
    } 

    let gapCol = 20;
    let x = this.footer.x + 36 ;
    for (let i = 1; i <= 9; i++) {
      // let beginX = this.footer.x + 15 + gapCol  
      let y = this.footer.y + 77; 
      if ( i == 2) {
        x += 15;
      } else if (i == 3) {
        x += 25;
      } else if (i == 4) {
        x += 30;
      } else if (i == 5) {
        x += 20;
      } else if (i == 6) {
        x += 15
      } else if (i == 7) {
        x += 20 
      } else if (i == 8) {
        x += 20 
      } else if (i == 9) {
        x = width - 35
      }
      // console.log(`${i}x: ${x}`)
      const tmp = new Character({
        scene: this,
        x: x,
        y: y,
        key: TextureKeys.Characters.onPlatform[`${i}`],    
        key2: TextureKeys.Characters.onDrag[`${i}`],
        xRatio: xRatio,
        yRatio: yRatio,
        index: i
      })
      tmp.charDrag.displayWidth = this.dragWs[i - 1]
      tmp.charDrag.displayHeight = this.dragHs[i - 1]
      x = tmp.x + tmp.displayWidth / 2 + gapCol
      // console.log(`before ${i}x: ${x}`)
    }

    for (let i = 1; i <= 9; i++) {
      let card = this.add.image(
        330,
        424,
        TextureKeys.Characters.cards[`${i}`]
      ).setOrigin(0.5, 0.5)
      card.displayWidth *= 0.9 
      card.displayHeight *= 0.9 
      card.setVisible(false)
      this.cards.push(card)
    }

    // closeSign section
    this.closeSign = this.add.image(
      566,
      117,
      TextureKeys.Close
    ).setOrigin(0.5, 0.5).setInteractive({ cursor: 'pointer' })
    this.closeSign.displayWidth *= 0.9
    this.closeSign.displayHeight *= 0.9
    this.closeSign.setVisible(false)
    this.closeSign.on('pointerdown', (pointer: any) => {
      let card = this.cards[this.curMatchId]
      this.closeSign.setVisible(false)
      // card.setVisible(false)
      card.destroy()
    })

    this.closeSign.on('pointerup', (pointer: any) => {
      this.closeSign.setVisible(false)
    })

    this.timeUp = new Mask({
      scene: this,
      x: width / 2,
      y: height / 2,
      key: TextureKeys.TimeUp
    })

    this.input.on('drag', (pointer: any, gameObject: any, dragX: number, dragY: number) => {
      // gameObject.setScale(1.5)
      gameObject.x = dragX
      gameObject.y = dragY
    })

    this.input.on('dragstart', function(pointer: any, gameObject: any, dropped: any) {
      if (dropped) return
      if (gameObject.type === "Container") {
        gameObject.charDrag.setVisible(true)
        gameObject.charPlatform.setVisible(false)
      }
      gameObject.y = gameObject.input.dragStartY;
      gameObject.x = gameObject.input.dragStartX;
    });

    this.input.on('dragend', function(pointer: any, gameObject: any, dropped: any) {
      if (dropped) return
      if (gameObject.type === "Container") {
        gameObject.charDrag.setVisible(false)
        gameObject.charPlatform.setVisible(true)
      }
      gameObject.x = gameObject.input.dragStartX;
      gameObject.y = gameObject.input.dragStartY;
    });

    this.input.on('drop', (pointer: any, gameObject: any, dropZone: any) => {
      if (gameObject.name === dropZone.name) {
        console.log(gameObject)
        gameObject.x = dropZone.x;
        gameObject.y = dropZone.y + dropZone.displayHeight / 2;
        gameObject.input.enabled = false;
        this.curMatchId = Number(gameObject.name - 1)
        let card = this.cards[this.curMatchId]
        card.setVisible(true)
        this.closeSign.setVisible(true)
        this.countMatch++  
      } else {
        if (gameObject.type === "Container") {
          gameObject.charDrag.setVisible(false)
          gameObject.charPlatform.setVisible(true)
        }
        gameObject.x = gameObject.input.dragStartX;
        gameObject.y = gameObject.input.dragStartY;
      } 
    })

  }

  update() {
    // if (this.countMatch === 9) {
    //   this.timeUp.state = "win"
    // } else {
    //   this.timeUp.state = "lose"
    // }
  }


}
