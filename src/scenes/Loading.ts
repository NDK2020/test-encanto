
import Phaser from "phaser"
import GameSound from "../atoms/Sound"
import SceneKeys from "../constants/SceneKeys"
import TextureKeys from "../constants/TextureKeys"

export default class Loading extends Phaser.Scene {
  private background!: Phaser.GameObjects.Image
  private logoPg!: Phaser.GameObjects.Image
  private intro!: Phaser.GameObjects.Image
  private btnStart!: Phaser.GameObjects.Image
  private progressAddY: number = 0
  private soundContainer!: GameSound
  constructor() {
    super(SceneKeys.Loading)
  }

  preload() {
    let width = this.cameras.main.width;
    let height = this.cameras.main.height;
    this.background = this.add.image(0, 0, TextureKeys.PreloadBackground)
      .setOrigin(0,0)
    const xRatio = width / this.background.width
    const yRatio = height / this.background.height
    this.background.displayWidth = width
    this.background.displayHeight = height 

    this.add.image(width / 2, 108, TextureKeys.LogoEncanto).setOrigin(0.5, 0.5)
    this.add.image(width - 12, height - 8, TextureKeys.Legal)
      .setOrigin(1,1)
    this.logoPg = this.add.image(12, height - 8, TextureKeys.LogoPg)
      .setOrigin(0,1)
    this.logoPg.displayWidth = this.logoPg.width * xRatio
    this.logoPg.displayHeight = this.logoPg.height * yRatio 

    let progressBar = this.add.graphics().setDepth(1);
    let progressBox = this.add.graphics();
    let boxColor = Phaser.Display.Color.HexStringToColor('#5d3fd3').color
    progressBox.fillStyle(boxColor, 0.8);
    progressBox.fillRect(330 - 160, 720 + this.progressAddY, 320, 30);
    let loadingText = this.make.text({
      x: width / 2,
      y: 720 + 30 + 20,
      text: 'Loading...',
      style: {
        font: '20px monospace',
        color: '#ffffff'
      }
    });
    loadingText.setOrigin(0.5, 0.5);
    // this.load.image(TextureKeys.PreloadBackground, 'assets/bg_1.jpg')
    this.load.image(TextureKeys.GameBackground, 'assets/bg_2.jpg')
    this.load.image(TextureKeys.OverBackground, 'assets/bg_3.jpg')
    this.load.image(TextureKeys.GameFooter, 'assets/game_platform.png')
    this.load.image(TextureKeys.SoundOff, 'assets/sound_off.png')
    this.load.image(TextureKeys.SoundOn, 'assets/sound_on.png')
    this.load.image(TextureKeys.Time, 'assets/time.png')
    this.load.image(TextureKeys.Close, 'assets/close.png')
    this.load.image(TextureKeys.TimeUp, 'assets/txt_timesup.png')
    this.load.image(TextureKeys.TxtIntro, 'assets/txt_intro.png')
    this.load.image(TextureKeys.TxtPrelaunch, 'assets/txt_prelaunch.png')
    this.load.image(TextureKeys.TxtTutorial, 'assets/txt_tutorial.png')
    this.load.image(TextureKeys.TxtLose, 'assets/txt_endscreen_lose.png')
    this.load.image(TextureKeys.TxtWin, 'assets/txt_endscreen_win.png')
    this.load.image(TextureKeys.BtnStart, 'assets/btn_start.png')
    this.load.image(TextureKeys.BtnReplay, 'assets/btn_replay.png')
    this.load.image(TextureKeys.BtnLearnMore, 'assets/btn_learnmore.png')
    this.load.image(TextureKeys.VideoFrame, 'assets/videoframe.png')
    this.load.video(
      TextureKeys.VideoPostLaunch,
      'assets/au_encanto_postlaunch.mp4'
    )

    for (let i = 1; i <= 9; i++) {
      let src = `assets/character/on_platform/${i}.png`
      this.load.image(TextureKeys.Characters.onPlatform[`${i}`], src)

      src = `assets/character/game/static/${i}.png`
      this.load.image(TextureKeys.Characters.onGame[`${i}`], src)

      src = `assets/character/game/drag/${i}.png`
      this.load.image(TextureKeys.Characters.onDrag[`${i}`], src)

      src = `assets/character_cards/popup_${i}.png`
      this.load.image(TextureKeys.Characters.cards[`${i}`], src)
    }

    this.load.on('progress', (value: any) => {
      // console.log(value)
      progressBar.clear()
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(330 - 156, 725 + this.progressAddY, 310 * value, 20)
    })

    this.load.on('fileprogress', (file: any) => {
      // console.log(file.src)
    })

    this.load.on('complete', () => {
      // console.log("compolete")
      progressBar.destroy()
      progressBox.destroy()
      loadingText.destroy()
      // progressBar.setVisible(false)
      // progressBox.setVisible(false)
      // loadingText.setVisible(false)
    })
  }


  create() {
    let width = this.cameras.main.width;
    let height = this.cameras.main.height;

    this.intro = this.add.image(
      width / 2, 680, TextureKeys.TxtIntro
    ).setOrigin(0.5, 0.5)
    this.intro.displayWidth *= 0.9
    this.intro.displayHeight *= 0.9

    this.soundContainer = new GameSound({
      scene: this,
      x: 616,
      y: 69,
      onKey: TextureKeys.SoundOn,
      offKey: TextureKeys.SoundOff,
      audioKey: 'soundtrack'
    })
    console.log("soundAtLoading: ", this.soundContainer)

    this.btnStart = this.add.image(
      width / 2, 780, TextureKeys.BtnStart
    ).setOrigin(0.5, 0.5).setInteractive({cursor: "pointer"})
    this.btnStart.displayWidth *= 0.9
    this.btnStart.displayHeight *= 0.9
    // this.scene.start(SceneKeys.Game)
    this.btnStart.on("pointerdown", (pointer: any) => {
      this.soundContainer.soundtrack.destroy()
      this.soundContainer.destroy()
      this.scene.start(SceneKeys.Game)
    })
  }

}

