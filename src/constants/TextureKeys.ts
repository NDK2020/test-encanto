type FreeObj = {[key: string]: any}
type Characters = {
  onPlatform: FreeObj,
  onGame: FreeObj,
  onDrag: FreeObj,
  cards: FreeObj
}

let characters: Characters = {
  onPlatform: {} ,
  onGame: {},
  onDrag: {},
  cards: {}
}

for (let i = 1; i <= 9; i++) {
  characters.onPlatform[`${i}`] = `characters_onPlatform_${i}`
  characters.onGame[`${i}`] = `characters_onGame_${i}`
  characters.onDrag[`${i}`] = `characters_onDrag_${i}`
  characters.cards[`${i}`] = `characters_cards_${i}`
}

const TextureKeys = {
  GameBackground : 'game-background',
  PreloadBackground : 'preload-background',
  OverBackground : 'over-background',
  GameFooter : 'game-footer',
  LogoPg : 'logo-pg',
  LogoEncanto : 'logo-encanto',
  Legal : 'legal',
  SoundOff : 'sound-off',
  SoundOn : 'sound-on',
  Time : 'time',
  TimeUp : 'time-up',
  Close: 'close-sign',
  BtnLearnMore: 'btn-learnmore',
  BtnReplay: 'btn-replay',
  BtnStart: 'btn-start',
  TxtIntro: 'txt-intro',
  TxtPrelaunch: 'txt-prelaunch',
  TxtTutorial: 'txt-tutorial',
  TxtLose: 'txt-lose',
  TxtWin: 'txt-win',
  VideoFrame: 'video-frame',
  VideoPostLaunch: 'video-post-launch',
  Characters: {...characters},
}
console.log(TextureKeys)


export default TextureKeys


