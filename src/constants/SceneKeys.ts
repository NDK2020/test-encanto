enum SceneKeys {
  Preloader = 'preloader',
  Loading = 'loading',
  Game = 'game',
  GameOver = 'game-over'
}

export default SceneKeys
