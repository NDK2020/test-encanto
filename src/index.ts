import Phaser from 'phaser';
import Game from './scenes/Game';
import Preloader from './scenes/Preloader';
import Loading from './scenes/Loading';
import GameOver from './scenes/GameOver';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  // parent: 'game',
  // backgroundColor: '#33A5E7',
  scale: {
    width: 660,
    height: 880,
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity:  {y: 0},
      debug: false
    }
  },
  scene: [Preloader, Loading, Game, GameOver]
};
export default new Phaser.Game(config)

