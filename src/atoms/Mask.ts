type Data = {
  scene: Phaser.Scene,
  x: number,
  y: number,
  key: string
}

export default class Mask extends Phaser.GameObjects.Container {
  public rect: Phaser.GameObjects.Graphics
  public img: Phaser.GameObjects.Image
  public text: Phaser.GameObjects.Text 
  public state: "win" | "lose"

  constructor(data: Data) {
    let { scene, x, y, key } = data;
    super(scene, x, y)
    const width = scene.scale.width
    const height = scene.scale.height
    
    this.img = scene.add.image(0, 0, key)
      .setOrigin(0.5,0.5).setVisible(false)

    this.text = scene.add.text(width / 2, height / 2, 'Congratulations!', {
      fontSize: '32px',
      color: 'yellow',
      shadow: {fill: true, blur: 0, offsetY: 0},
    }).setOrigin(0.5, 0.5).setDepth(3).setVisible(false)


    this.rect = scene.add.graphics()
      .setPosition(0, 0)
      .fillRect(0, 0, width, height)
      .setDepth(2)
    this.rect.fillStyle(0x000000, 0.5)
    this.img.setMask(this.rect.createGeometryMask())
    this.img.setVisible(false)
    this.rect.setVisible(false)
    this.setVisible(false)
    this.depth = 2
    this.add(this.img)

    // this.setSize(this.offSound.displayWidth, this.offSound.displayHeight)
    // this.setInteractive()
    this.scene = scene
    this.scene.add.existing(this)

  }
  // update () {
  //   if (this.state === "lose") {
  //     this.text.setVisible(true)
  //     this.img.setVisible(false)
  //   } else {
  //     this.text.setVisible(false)
  //     this.img.setVisible(true)
  //   }
  // }
}
