type Data = {
  scene: Phaser.Scene,
  x: number,
  y: number,
  key: string,
  key2: string,
  xRatio: number,
  yRatio: number,
  index: number
}

export default class Character extends Phaser.GameObjects.Container {
  public charPlatform: Phaser.GameObjects.Image
  public charDrag: Phaser.GameObjects.Image


  constructor(data: Data) {
    let {scene, x, y, key, key2, xRatio, yRatio, index } = data;
    super(scene, x, y)
    const width = scene.scale.width
    const height = scene.scale.height
    
    this.charPlatform = scene.add.image(0, 0, key)
      .setOrigin(0.5,0.5)
    // this.charPlatform.displayWidth = width * xRatio
    // this.charPlatform.displayHeight = height * yRatio

    this.charDrag = scene.add.image(0, 0, key2)
      .setOrigin(0.5,0.5)
    // this.charDrag.setTint(0x000000)

    // this.setSize(200, 200)
    this.setSize(this.charDrag.displayWidth, this.charDrag.displayHeight)
    this.charDrag.setVisible(false)

    this.add(this.charPlatform)
    this.add(this.charDrag)

    this.scene = scene
    this.scene.add.existing(this)
    // scene.input.enableDebug(this)

    this.name = `${index}`
    this.setInteractive()
    scene.input.setDraggable(this)
    scene.physics.add.existing(this)
    // this.on('pointerover',  () => {
    //   this.charDrag.setTint(0x44ff44);
    // });
    //
    // this.on('pointerout',  () => {
    //   this.charDrag.clearTint();
    // });
    // var graphics = scene.add.graphics();
    // graphics.lineStyle(2, 0x00ffff, 1);
    // graphics.strokeCircle(this.x, this.y, this.input.hitArea.radius);

  }
}
