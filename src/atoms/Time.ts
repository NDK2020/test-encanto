import SceneKeys from "../constants/SceneKeys";
import DemoScene from "../scenes/Game";

type Data = {
  scene: DemoScene,
  x: number,
  y: number,
  key: string,
  state: "pause" | "running" | "end",
  initTime: number
}

export default class Timer extends Phaser.GameObjects.Container {
  public bg: Phaser.GameObjects.Image
  public text: Phaser.GameObjects.Text 

  // const fortmatTime(t: number) {
  //   var
  // }

  constructor(data: Data) {
    let {scene, x, y, key, state } = data;
    super(scene, x, y)
    
    this.bg = scene.add.image(0, 0, key)
      .setOrigin(0.5,0.5)

    this.text = scene.add.text(x + 46, y - 6, String(scene.initTime), {
      fontSize: '32px',
      color: '#ffffff',
      shadow: {fill: true, blur: 0, offsetY: 0},
      padding: {left: 15, right: 15, top: 10, bottom: 10 }
    }).setOrigin(0.5).setDepth(1)

    // timer iplementation
    const onEvent = () => {
      scene.initTime -= 1; // One second
        // console.log("timer: ", scene.initTime)
      if (scene.initTime <= 0) {
        scene.timeUp.setVisible(true)
        scene.timeUp.rect.setVisible(true)
        if (scene.countMatch === 9) {
          scene.timeUp.img.setVisible(false)
          scene.timeUp.text.setVisible(true)
        } else {
          scene.timeUp.img.setVisible(true)
          scene.timeUp.text.setVisible(false)
        }
        this.text.setText('00');
        this.text.setDepth(0)
        if (scene.initTime === -3) {
          scene.soundContainer.soundtrack.destroy()
          scene.soundContainer.destroy()
          scene.scene.start(SceneKeys.GameOver, {countMatch: scene.countMatch})
          scene.time.paused
          scene.initTime = 30
        }
      } else {
        this.text.setText(String(scene.initTime));
      }
    }
    // Each 1000 ms call onEvent
    let timedEvent = scene.time.addEvent({ delay: 1000, callback: onEvent, callbackScope: this, loop: true })

    this.add(this.bg)
    this.scene = scene
    this.scene.add.existing(this)

  }
}
