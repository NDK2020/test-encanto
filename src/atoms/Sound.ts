
type Data = {
  scene: Phaser.Scene,
  x: number,
  y: number,
  onKey: string,
  offKey: string,
  audioKey: string
}

export default class GameSound extends Phaser.GameObjects.Container {
  public onSound: Phaser.GameObjects.Image
  public offSound: Phaser.GameObjects.Image
  public soundtrack: Phaser.Sound.BaseSound

  constructor(data: Data) {
    let { scene, x, y, onKey, offKey } = data;
    super(scene, x, y)
    this.scene = scene
    
    this.onSound = scene.add.image(0, 0, onKey)
      .setOrigin(0.5,0.5)

    this.offSound = scene.add.image(0, 0, offKey)
      .setOrigin(0.5,0.5)
    this.offSound.setVisible(false)
    this.soundtrack = scene.sound.add('soundtrack')
    this.soundtrack.play( {loop: true})
    // console.log(soundtrack.isPlaying)

    this.add(this.onSound)
    this.add(this.offSound)

    this.setSize(this.offSound.displayWidth, this.offSound.displayHeight)
    this.setInteractive()
    this.scene = scene
    this.scene.add.existing(this)

    // console.log("onSound: ", this.onSound.visible)
    // console.log("offSound: ", this.offSound.visible)
    this.on('pointerdown', (pointer) => {
      this.onSound.setVisible(!this.onSound.visible)
      this.offSound.setVisible(!this.offSound.visible)
      if (this.onSound.visible) {
        this.soundtrack.play()
      } else {
        this.soundtrack.stop()
      }
    })
  }

}
