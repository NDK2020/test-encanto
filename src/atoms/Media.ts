type Data = {
  scene: Phaser.Scene,
  x: number,
  y: number,
  frameKey: string,
  mediaKey: string,
}

export default class Media extends Phaser.GameObjects.Container {
  public frame: Phaser.GameObjects.Image
  public media: Phaser.GameObjects.Video

  constructor(data: Data) {
    let { scene, x, y, frameKey, mediaKey } = data;
    super(scene, x, y)
    
    this.frame = scene.add.image(0, 0, frameKey)
      .setOrigin(0.5,0.5)

    this.media = scene.add.video(0, 0, mediaKey)
      .setOrigin(0.5,0.5)
    this.media.play(false)
    this.media.video.autoplay = true
    this.media.video.setAttribute('autoplay', 'autoplay')


    this.media.displayWidth = this.frame.displayWidth - 18 
    this.media.displayHeight = this.frame.displayHeight - 18 

    this.add(this.frame)
    this.add(this.media)

    this.setSize(this.frame.displayWidth, this.frame.displayHeight)
    this.setInteractive()
    this.on("pointerdown", (pointer) => {
      // console.log(this.media)
      let isPause = this.media.video.paused
      if(isPause) {
        this.media.video.play()
      } else {
        this.media.video.pause()
      }
    })
    this.scene = scene
    this.scene.add.existing(this)

  }
}
